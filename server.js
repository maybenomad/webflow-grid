var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var config = require('./webpack.config');

const PORT = 3000

new WebpackDevServer(webpack(config), config.devServer)
  .listen(PORT, 'localhost', function (err, result) {
    if (err) { console.log(err) }
  })

# Webflow Grid Thing
![relevant gif?](http://i.giphy.com/vYtIhwaPVejPq.gif)

```
npm install
npm start
```

### Why I Used...
[**Mori**](https://github.com/swannodette/mori)
Immutable data structures. I chose this over Immutable because it
is an order of magnitude faster and offers a pure functional API versus a
chaining one at the cost of a very steep learning curve.

[**js-csp**](https://github.com/ubolonton/js-csp)
Communicating sequential processes implemented via ES6 generators.
CSPs are an extremely powerful tool for managing complex UI interactions and
offer innumerable benefits over other options for asynchronous work.

### The Grid Resizer
`src/components/tools/column.react.js`
The UI logic lives here, as the core of the feature really revolves around columns.

`src/components/tools/grid_resizer.js`
The resizer component, but it mostly just renders a bunch of columns and tells
them that they're being used for resizing. Should really be in its own file,
but it is the only `<ActiveTool />` component at the moment so I didn't bother.

`src/processes/resizer.js`
The interaction logic completely decoupled from the actual grid resizer. This
process could be applied to resizing any DOM element without modification. I
wanted to build a demo of using it for images but didn't have time.

`src/mutators/grid_resize.js`
The grid-specific resizing logic. This is instantiated and passed to the
resizer process to give it its behavior.

If you want to know more about this `mutators` and `processes` scheme I'd be
happy to oblige. It's an architecture that I'm iterating on every day, but I
believe offers many advantages over the vanilla Flux prescribed by Facebook.

Anyways, have fun and stuff.

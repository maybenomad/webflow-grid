import * as _ from 'lib/collections'

const EDITABLE_CLASSES = [
  'w-container',
  'w-section',
  'w-row',
  'w-col'
]

const EDITABLE_TAGS = [
  'h1',
  'h2',
  'h3',
  'a',
  'p',
  'div'
]

export function isEditable(node) {
  return EDITABLE_TAGS.includes(m.get(node, 'tag')) ||
         EDITABLE_CLASSES.includes(m.getIn(node, ['@class', 0]))
}

export function getValueType(node) {
  if (containsHTML(node)) {
    return 'html'
  }
  else if (containsText(node)) {
    return 'text'
  }
}

export function getClassNames(node) {
  const classes = m.get(node, '@class')
  return classes ? _.join(classes) : ''
}

export function getGrid(node) {
  const grid = m.getIn(node, [ 'data', 'grid', 'cols', 'main' ])
  return grid.split('|').map(Number)
}

export function getMainClass(node) {
  return m.getIn(node, ['@class', 0])
}

function containsHTML(node) {
  return /.*\<[^>]+>.*/.test(m.get(node, 'v'))
}

function containsText(node) {
  return !!m.get(node, 'v')
}

import t from 'transducers-js'

/*
 * A giant bag of random helper functions because why not.
 */

export function selectValues(map, keyseq) {
  return _.map(_.partial(_.get, map), keyseq)
}

export function selectKeys(map, keyseq) {
  return _.zipmap(keyseq, _.selectValues(map, keyseq))
}

export function isArray(value) {
  return value.constructor === Array
}

export function isNull(value) {
  return value === null
}

export function isUndefined(value) {
  return value === undefined
}

export function areEqual(a, b) {
  return m.equals(m.toClj(a), m.toClj(b))
}

export function mapWithIndex(f, coll) {
  const count = m.range(m.count(coll))
  return m.map((x) => f(m.get(x, 0), m.get(x, 1)),
               m.zipmap(count, coll))
}

export function findOne(coll, key, value) {
  return m.some(e => m.equals(m.get(e, key), value) && e, coll)
}

export function dropRight(n, coll) {
  const count = m.count(coll)
  return m.take(count - n, coll)
}

export function dirtyMap(coll, f) {
  return m.intoArray(m.vector(), m.map(f, coll))
}

export function findIndex(coll, key, value) {
  return m.toJs(m.map(x => m.get(x, key), coll)).indexOf(value)
}

export function pluck(coll, key) {
  return m.map(e => m.get(e, key), coll)
}

export function indexByKey(coll, key='_id') {
  return m.zipmap(pluck(coll, key), coll)
}

export function indexAndMerge(map, newElements) {
  return m.into(map, indexByKey(newElements))
}

export function getByIndexes(map, indexes) {
  return m.map(m.partial(m.get, map), indexes)
}

export function join(coll, sep=' ') {
  return m.reduce((acc, e) => acc + e, '', m.interpose(sep, coll))
}

export function compact(coll) {
  return m.remove(isNull, coll)
}

import { comp, map, into } from 'transducers-js'
import _ from 'lodash'

function kebabToCamel(str) {
  return str.replace(/-(.)/g, (_, ch) => ch.toUpperCase())
}

export function stringToObject(cssString) {
  // Chomp off trailing semicolon and split rules.
  const styles = cssString
    .replace(/;$/, '')
    .split(';')

  const split = (rule) => rule.split(':')
  const camelCase = ([ key, value ]) => [ kebabToCamel(key), value ]

  const xf = comp(map(split), map(camelCase))
  return into({}, xf, styles)
}

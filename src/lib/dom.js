function _ensureParent(node, parent) {
  return parent || node.parentNode
}

/*
 * Returns the provided element's offset relative to the top left
 * of the entire document.
 */
function _getAbsoluteOffset(element, top=0, left=0) {
  if (!element) {
    return { top, left }
  }

  return _getAbsoluteOffset(
    element.offsetParent,
    top + element.offsetTop,
    left + element.offsetLeft
  )
}

/*
 * Returns the provided element's offset relative to its parent.
 */
function _getRelativeOffset(element, parent) {
  parent = _ensureParent(element, parent)

  return {
    top: element.offsetTop - parent.offsetTop,
    left: element.offsetLeft - parent.offsetLeft
  }
}

export function isFirstChild(element, parent) {
  parent = _ensureParent(element, parent)
  return element === parent.firstChild
}

export function isLastChild(element, parent) {
  parent = _ensureParent(element, parent)
  return element === parent.lastChild
}

/*
 * Returns basic spatial information and DOM references for the
 * provided node id. If isAbsolute is true, the returned offset
 * will be from the top of the document, else it will be from the
 * node's parent.
 */
export function getCanvasElement(id, isAbsolute) {
  const node = document.getElementById(id)
  const parent = node.parentNode
  const offset = isAbsolute ? _getAbsoluteOffset(node) :
                              _getRelativeOffset(node, parent)

  return {
    node,
    parent,
    bounds: {
      ...offset,
      width: node.offsetWidth,
      height: node.offsetHeight
    }
  }
}


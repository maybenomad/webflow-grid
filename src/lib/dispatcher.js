import { into, partitionAll, wrap } from 'transducers-js'
import atom from 'lib/atom'
import cursor from 'lib/cursor'

function transducerFromReducers(coll) {
  const reducers = into({}, partitionAll(2), coll)

  return wrap(function(result, [type, value]) {
    if (reducers[type]) {
      return reducers[type](result, value)
    }
    return result
  })
}

function dispatcher() {
  let state = atom(m.vector())
  let watchers = m.vector()

  function tap(initial, ...xf) {
    if (typeof m.first(xf) === 'symbol') {
      xf = transducerFromReducers(xf)
    }
    xf = typeof xf === 'function' ? wrap(xf) : xf

    const key = m.count(state.deref())
    state.swap(m.conj, initial)
    watchers = m.conj(watchers, xf)

    return cursor(state, m.vector(key))
  }

  function dispatch(type, data) {
    data = m.toClj(data)

    function reducer(state, key, xf) {
      return m.assoc(state,
                     key,
                     xf["@@transducer/step"](m.get(state, key), [type, data]))
    }

    state.swap(m.partial(m.reduceKV, reducer), watchers)
  }

  return { tap, dispatch }
}

export default dispatcher()

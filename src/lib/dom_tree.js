import * as _ from 'lib/collections'

/*
 * Constructs the DOM tree from a list of initial nodes. Not tail call
 * recursive because I'm lazy.
 */
export function build(nextState) {
  const indexed = _.indexAndMerge(m.hashMap(), nextState)
  const getChildren = (node) => _.getByIndexes(indexed, m.get(node, 'children'))

  function recur(node) {
    const children = m.into(m.vector(), m.map(recur, getChildren(node)))
    return m.assoc(node, 'children', children)
  }

  const body = _.findOne(nextState, 'tag', 'body')
  return recur(body)
}

/*
 * Constructs the DOM tree index, a map with node ids as keys and full keyPaths
 * into the DOM tree as values. Also not tail call recursive because I'm lazy.
 */
export function buildIndex(tree) {
  function recur(tree, keyPath) {
    const index = m.hashMap(m.get(tree, '_id'), keyPath)
    const reducer = (acc, key, node) => {
      return m.conj(acc, recur(node, m.conj(keyPath, key)))
    }

    return m.reduceKV(reducer, index, m.get(tree, 'children'))
  }

  return recur(tree, m.vector())
}

/*
 * Retrieve the full keyPath using the index's reduced version.
 */
export function resolveIndexKeyPath(keyPath) {
  if (m.isEmpty(keyPath)) return m.vector()
  return m.conj(m.interpose('children', keyPath), 'children')
}

/*
 * Retrieve the full keyPath using a node's id.
 */
export function resolveIndex(index, id) {
  return m.into(m.vector(),
                resolveIndexKeyPath(m.get(index, id)))
}


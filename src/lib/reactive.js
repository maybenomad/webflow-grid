import csp from 'js-csp'

/*
 * Creates a channel that acts as an event listener for events of the provided
 * `type` occurring on the supplied element `el`. If a transducer is supplied,
 * it will be applied to the channel.
 *
 * When the channel is closed, the event listener will also close.
 */
export function listen(el, type, xf, preventDefault) {
  let chan = csp.chan(1, xf)
  el.addEventListener(type, function listener(e) {
    if (preventDefault) {
      e.preventDefault()
    }
    if (!csp.putAsync(chan, e)) {
      el.removeEventListener(listener)
    }
  })
  return chan
}

export default function cursor(atom, keyPath) {

  function watch(fn) {
    return atom.watch(keyPath, fn)
  }

  function deref() {
    return m.getIn(atom.deref(), keyPath)
  }

  function get(key) {
    if (!key) return this
    return cursor(atom, m.conj(keyPath, key))
  }

  function getIn(path) {
    return cursor(atom, m.into(keyPath, path))
  }

  return { watch, deref, get, getIn }

}

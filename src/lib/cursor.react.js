export default function cursor(name, fn) {
  return function(Component) {

    class CursorDecorator extends React.Component {

      componentWillMount() {
        const cursor = fn(this.props)
        this._unwatchCursor = cursor.watch(this._setStateWithCursor.bind(this))
        this._setStateWithCursor(cursor.deref())
      }

      componentWillUnmount() {
        this._unwatchCursor()
      }

      render() {
        return <Component {...this.props} {...this.state} />
      }

      _setStateWithCursor(nextState) {
        this.setState({ [name]: nextState })
      }

    }

    return CursorDecorator

  }
}

export function createProcess(processFn, eventFn) {
  return function(target, mutator) {
    const inCh = eventFn(target)
    return { out: processFn(inCh, mutator), kill: inCh }
  }
}

const MAX_WATCHERS = 100

export default function atom(state) {

  let watchers = m.hashMap()
  let availableWatcherKeys = m.into(m.vector(), m.range(MAX_WATCHERS))

  function _watch(fn) {
    const key = m.peek(availableWatcherKeys)
    availableWatcherKeys = m.pop(availableWatcherKeys)

    watchers = m.assoc(watchers, key, fn)
    return key
  }

  function _notify() {
    m.each(m.vals(watchers), updater => updater(state))
  }

  function _unwatch(key) {
    watchers = m.dissoc(watchers, key)
    availableWatcherKeys = m.conj(availableWatcherKeys, key)
  }

  function reset(nextState) {
    state = nextState
    _notify()
    return state
  }

  function compareAndSet(nextState) {
    if (!m.equals(state, nextState)) {
      return reset(nextState)
    }
    return state
  }

  function swap(fn, ...args) {
    return compareAndSet(fn(state, ...args))
  }

  function deref() {
    return state
  }

  function watch(keyPath, fn) {
    const updater = m.comp(fn, m.curry(m.getIn, keyPath))
    const key = _watch(updater)

    return function unwatch() {
      _unwatch(key)
    }
  }

  return {
    compareAndSet, reset, swap,
    deref,
    watch
  }

}

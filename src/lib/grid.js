export const GRID_SIZE = 12

export function isValid(grid) {
  return sum(grid) === GRID_SIZE && grid.every(c => c > 0)
}

function sum(arr) {
  return arr.reduce((a, b) => a + b)
}

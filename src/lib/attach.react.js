import csp from 'js-csp'

export function attach(process, eventState, mutator) {
  return function(Component) {

    class AttachDecorator extends React.Component {

      componentWillMount() {
        if (eventState) {
          this.setState(eventState)
        }
      }

      componentDidMount() {
        // Start the process and save its output channels.
        this._channels = process(this, mutator ? mutator(this.props) : null)

        const validEvents = Object.keys(eventState)
        const inCh = this._channels.out

        csp.go(function* () {
          while (true) {
            const event = yield inCh
            if (event === csp.CLOSED) {
              break
            }

            // Merge events output by the process into component state.
            const [key, value] = event
            if (validEvents.includes(key)) {
              this.setState({ [key]: value })
            }
          }
        }.bind(this))
      }

      componentWillUnmount() {
        this._channels.kill.close()
      }

      render() {
        return <Component {...this.props} {...this.state} />
      }

    }

    return AttachDecorator
  }
}

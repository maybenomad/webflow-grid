import { getGrid } from 'lib/dom_node'
import * as Grid from 'lib/grid'
import Action from 'actions/constants'
import * as _ from 'lib/collections'
import dispatcher from 'lib/dispatcher'

export default function gridResizer({ node: rowNode, ctx }) {

  const grid_ = getGrid(rowNode)
  const columnIndex_ = _getColumnIndex()
  const threshold_ = m.get(ctx, 'columnWidth') / grid_[columnIndex_]

  function _getColumnIndex() {
    const columnId = m.get(ctx, 'columnId')
    const index = _.findIndex(m.get(rowNode, 'children'), '_id', columnId)

    // Subtract one if the left gutter was clicked.
    return m.get(ctx, 'left') ? index - 1 : index
  }

  return {
    startResize() {},

    attemptResize({ x: mouseX }) {
      const delta = m.get(ctx, 'initialX') - mouseX
      const normalizedDelta = Math.round(delta / threshold_)
      const grid = getGrid(rowNode)

      grid[columnIndex_] -= normalizedDelta
      grid[columnIndex_ + 1] += normalizedDelta

      if (Grid.isValid(grid)) {
        dispatcher.dispatch(Action.RESIZED_DOM_COLUMN, {
          rowId: m.get(rowNode, '_id'), grid
        })
      }
    },

    endResize() {
      dispatcher.dispatch(Action.QUIT_ACTIVE_TOOL)
    }
  }
}

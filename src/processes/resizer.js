import * as reactive from 'lib/reactive'
import { map } from 'transducers-js'
import csp from 'js-csp'
import { mouseMoveEvents, positionTracker } from 'processes/positionTracker'

/**
 * Handles resize actions which rely on movement tracking.
 *
 * Protocol for mutator:
 *  - startResize(x, y)
 *  - endResize()
 *  - attemptResize(x, y)
 */
export function resizer(inCh, mutator) {
  const out = csp.chan()

  csp.go(function* () {
    mutator.startResize()

    while(true) {
      let event = yield inCh
      if (event === csp.CLOSED) {
        break
      }

      let [type, data] = event
      if (type === 'end_resize') {
        mutator.endResize()
        break
      }
      else if (type === 'move') {
        mutator.attemptResize(data)
      }
      else {
        yield csp.put(out, event)
      }
    }

    out.close()
  })

  return out
}

/**
 * Returns a channel that maps mouseup events to cancel events.
 */
export function resizerEvents() {
  const cancelEvent = m.constantly(['end_resize'])
  return reactive.listen(document, 'mouseup', map(cancelEvent))
}

/**
 * A resizer that coordinates click events and mouse position to resize
 * DOM elements.
 */
export function DOMNodeResizer(target, mutator) {
  let mouseCh = mouseMoveEvents()
  let resizerCh = resizerEvents()
  let inCh = csp.operations.merge([ mouseCh, resizerCh ])

  let out = resizer(positionTracker(inCh), mutator)
  return { out, kill: mouseCh }
}

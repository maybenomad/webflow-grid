import csp from 'js-csp'
import * as reactive from 'lib/reactive'
import { map } from 'transducers-js'
import { createProcess } from 'lib/process'

// Tuples describing the events this process is capable of producing on its
// output channel.
export const defaults = [
  ['move', { x: 0, y: 0 }]
]

/**
 * Normalizes movement events over a 2D plane.
 */
export function positionTracker(inCh) {
  const out = csp.chan()

  csp.go(function* () {
    while (true) {
      let event = yield inCh
      if (event === csp.CLOSED) {
        break
      }

      yield csp.put(out, event)
    }

    out.close()
  })

  return out
}


const mouseMoveEvent = (e) => ['move', { x: e.clientX, y: e.clientY }]

/*
 * DOM event channel generators.
 */
export function mouseMoveEvents() {
  return reactive.listen(document, 'mousemove', map(mouseMoveEvent))
}

/**
 * Tracks mouse cursor movement across the document.
 */
export const MouseTracker = createProcess(positionTracker, mouseMoveEvents)

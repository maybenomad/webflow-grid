require('babel/polyfill')
require('!style!css!./css/bootflow.css')

import dispatcher from 'lib/dispatcher'

// Ensure all stores are initialized since they're singletons and junk.

import DOMNodeStore from './stores/dom_nodes'
import DOMStyleStore from './stores/dom_styles'
import CanvasStore from './stores/canvas'

import CanvasActions from 'actions/canvas'

CanvasActions.bootstrap()

import App from './components/app.react'

React.render(
  React.createElement(App),
  document.body
)

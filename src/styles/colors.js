function rgba(r, g, b, a=1) {
  return `rgba(${r},${g},${b},${a})`
}

export function border(width, style, color) {
  width = typeof width === 'string' ? width : `${width}px`
  return `${width} ${style} ${color}`
}

export const ToolColor = {
  Selected: rgba(0, 127, 255, 0.65),
  Hovered: rgba(0, 127, 255, 0.3)
}

import Action from 'actions/constants'
import * as CanvasDOM from 'stores/dom_nodes'
import dispatcher from 'lib/dispatcher'

/*
 * Tracks UI state regarding the canvas that must be shared between disparate
 * components.
 */
const canvasState = m.hashMap(
  'selectedTarget', null,
  'hoverTarget', null,
  'activeToolTarget', null,
  'activeToolContext', null
)

const cursor = dispatcher.tap(
  canvasState,

  Action.RECEIVED_CANVAS_CLICK, setFromDOM('selectedTarget'),
  Action.RECEIVED_CANVAS_HOVER, setFromDOM('hoverTarget'),

  Action.CHANGED_ACTIVE_TOOL,
  function changedActiveTool(state, data) {
    const target = CanvasDOM.getById(m.get(data, 'targetId'))
    return m.merge(state, m.hashMap('activeToolTarget', target,
                                    'activeToolContext', m.get(data, 'ctx')))
  },

  Action.QUIT_ACTIVE_TOOL,
  function quitActiveTool(state) {
    return m.merge(state, m.hashMap('activeToolTarget', null,
                                    'activeToolContext', null))
  }
)

function setFromDOM(target) {
  return (state, targetId) => {
    const node = CanvasDOM.getById(targetId)
    return m.assoc(state, target, node)
  }
}

export default cursor

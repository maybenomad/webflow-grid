import Action from 'actions/constants'
import dispatcher from 'lib/dispatcher'
import * as _ from 'lib/collections'
import { stringToObject } from 'lib/refines_styles'

/*
 * Stores style nodes and constructs class strings very poorly and
 * inefficiently.
 */
const cursor = dispatcher.tap(
  m.hashMap(),

  Action.RECEIVED_DOM_STYLES, _.indexAndMerge
)

export function getForNode(node) {
  const pluckStyles = m.curry(m.getIn, ['data', 'styleLess'])

  let classes = m.pipeline(
    m.get(node, 'classes'),
    m.partial(_.getByIndexes, cursor.deref()),
    m.partial(m.map, pluckStyles),
    _.compact,
    m.partial(m.map, stringToObject),
    m.partial(m.reduce, Object.assign, {})
  )

  return classes
}

export default cursor

import Action from 'actions/constants'
import atom from 'lib/atom'
import dispatcher from 'lib/dispatcher'
import * as _ from 'lib/collections'
import * as DOMTree from 'lib/dom_tree'

const nodeIndex = atom(m.hashMap())

/**
 * An actual tree structure mirroring the DOM which is constructed out of
 * node data.
 */
const nodeTree = dispatcher.tap(
  m.hashMap(),

  Action.RECEIVED_DOM_NODES,
  function constructTree(state, nextState) {
    const tree = DOMTree.build(nextState)
    nodeIndex.reset(DOMTree.buildIndex(tree))
    return tree
  },

  Action.RESIZED_DOM_COLUMN, updateGrid
)

/*
 * Update a row's grid layout and apply the change to its children's CSS rules.
 */
function updateGrid(tree, event) {
  const { rowId, grid } = m.toJs(event)
  const gridKeyPath = ['data', 'grid', 'cols', 'main']
  tree = updateNode(tree, rowId, m.curry(m.assocIn, gridKeyPath, _.join(grid, '|')))

  return updateChildren(tree, rowId, (i, child) => {
    const newRule = `w-col-${m.get(grid, i)}`
    return m.assocIn(child, ['@class', 1], newRule)
  })
}

/*
 * Returns the tree resulting from applying fn to node `id`.
 */
function updateNode(state, id, fn) {
  const keyPath = DOMTree.resolveIndex(nodeIndex.deref(), id)
  return m.updateIn(state, keyPath, fn)
}

/*
 * Returns the tree resulting from applying fn to each child node of node `id`.
 */
function updateChildren(state, parentId, fn) {
  const indexKeyPath = DOMTree.resolveIndex(nodeIndex.deref(), parentId)
  const keyPath = m.conj(indexKeyPath, 'children')
  return m.assocIn(state,
                   keyPath,
                   m.into(m.vector(),
                          _.mapWithIndex(fn, m.getIn(state, keyPath))))
}

/*
 * Helper for reading values from the store without a cursor.
 */
export function getById(id) {
  return m.getIn(nodeTree.deref(),
                 DOMTree.resolveIndex(nodeIndex.deref(), id))
}

export default nodeTree

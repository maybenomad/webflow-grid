import CanvasDOM, { getById } from 'stores/dom_nodes'
import cursor from 'lib/cursor.react'
import * as _ from 'lib/collections'
import CanvasActions from 'actions/canvas'
import * as DOMUtils from 'lib/dom_node'

import DOMNode from 'components/canvas/dom_node.react'

@cursor('root', () => CanvasDOM.get())
class Canvas extends React.Component {
  render() {
    const root = this.props.root

    return (
      <div style={ styles } {...eventHandlers}>
        { _.dirtyMap(m.get(root, 'children'), node => {
            return <DOMNode key={ node._id } node={ node } />
          })
        }
      </div>
    )
  }
}

const eventHandlers = {

  onClick(e) {
    e.preventDefault()
    const node = getById(e.target.id)
    if (DOMUtils.isEditable(node)) {
      CanvasActions.clicked(e.target.id)
    }
  },

  onMouseOver(e) {
    e.stopPropagation()
    const node = getById(e.target.id)
    if (DOMUtils.isEditable(node)) {
      CanvasActions.changedHoverTarget(e.target.id)
    }
  }

}

const styles = {
  width: '100%',
  height: '100%',
  cursor: 'default',
  overflowY: 'scroll'
}

export default Canvas

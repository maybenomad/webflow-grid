import * as DOMNodes from 'stores/dom_nodes'
import * as DOMStyles from 'stores/dom_styles'
import * as DOMUtils from 'lib/dom_node'
import * as _ from 'lib/collections'

/*
 * Recursively renders a DOMNode data element to build the entire page.
 */
class DOMNode extends React.Component {

  shouldComponentUpdate(nextProps) {
    return !m.equals(this.props.node, nextProps.node)
  }

  render() {
    const node = this.props.node

    const props = {
      id: m.get(node, '_id'),
      style: DOMStyles.getForNode(node),
      className: DOMUtils.getClassNames(node)
    }

    const children = _.dirtyMap(m.get(node, 'children'), child => {
      return <DOMNode key={ m.get(child, '_id') } node={ child } />
    })

    return React.createElement(
      m.get(node, 'tag'),
      props,
      m.get(node, 'v') ? <ValueNode node={node} /> : children
    )
  }

}

class ValueNode extends React.Component {
  render() {
    const node = this.props.node

    if (DOMUtils.getValueType(node) === 'html') {
      return <HTMLValueNode node={node} />
    }
    return <span style={{ pointerEvents: 'none' }}>{ m.get(node, 'v') }</span>
  }
}

class HTMLValueNode extends React.Component {
  render() {
    const node = this.props.node
    return <div id={ m.get(node, 'id') }
                dangerouslySetInnerHTML={{ __html: m.get(node, 'v') }} />
  }
}

export default DOMNode

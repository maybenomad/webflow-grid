import WebflowNode from 'components/tools/webflow_node.react'

export default class SelectedElement extends React.Component {
  render() {
    return <WebflowNode absolute selected node={ this.props.node } />
  }
}

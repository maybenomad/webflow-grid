import CanvasActions from 'actions/canvas'
import WebflowNode from 'components/tools/webflow_node.react'
import { MouseTracker } from 'processes/positionTracker'
import { attach } from 'lib/attach.react'
import * as DOM from 'lib/dom'
import { ToolColor, border } from 'styles/colors'

@attach(MouseTracker, { move: { x: 0, y: 0 } })
export default class Column extends React.Component {
  render() {
    const id = m.get(this.props.node, '_id')
    const canvasNode = DOM.getCanvasElement(id, true)

    return (
      <WebflowNode hovered canvasNode={ canvasNode }>
        { !DOM.isFirstChild(canvasNode.node, canvasNode.parent) ?
          <ResizeGutter left {...this.props} canvasNode={ canvasNode } /> : null
        }
        { !DOM.isLastChild(canvasNode.node, canvasNode.parent) ?
          <ResizeGutter right {...this.props} canvasNode={ canvasNode } /> : null
        }
      </WebflowNode>
    )
  }
}

class ResizeGutter extends React.Component {
  render() {
    return <div style={ gutterStyles(this.props) }
                onMouseDown={ startResize(this.props) } />
  }
}

const startResize = (props) => {
  return (e) => {
    e.preventDefault()
    CanvasActions.changedActiveTool(props.canvasNode.parent.id, {
      columnId: props.canvasNode.node.id,
      initialX: props.move.x,
      columnWidth: props.canvasNode.bounds.width,
      left: props.left
    })
  }
}

// The width of the gutter in pixels which effectively determines its
// horizontal activation range relative to the cursor.
const GUTTER_SENSITIVITY = 10

// Increases the gutter's opacity relative to how close the mouse is.
function getGutterOpacity(offset, mousePosition) {
  const distanceFromGutter = Math.abs(offset - mousePosition.x)
  return (GUTTER_SENSITIVITY - distanceFromGutter) / GUTTER_SENSITIVITY
}

const gutterStyles = (props) => {
  // A case study in how many variables you can destructure out into one scope.
  const {
    left,
    right,
    move: mousePosition,
    canvasNode: {
      parent,
      bounds: {
        left: leftBound,
        width
      }
    }
  } = props

  const rightBound = leftBound + width
  const borderStyle = border(4, 'dotted', ToolColor.Hovered)

  return {
    position: 'absolute',

    height: parent.offsetHeight,
    width: GUTTER_SENSITIVITY,

    top: -1,

    right: right ? -3 : 'auto',
    left: left ? -3 : 'auto',

    borderRight: right ? borderStyle : 0,
    borderLeft: left ? borderStyle : 0,

    pointerEvents: 'auto',
    cursor: 'col-resize',

    opacity: getGutterOpacity(left ? leftBound : rightBound, mousePosition)
  }
}

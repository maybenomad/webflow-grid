import cursor from 'lib/cursor.react'
import CanvasEditor from 'stores/canvas'

import HoveredElement from 'components/tools/hovered_element.react'
import SelectedElement from 'components/tools/selected_element.react'
import ActiveTool from 'components/tools/active_tool.react'


@cursor('canvas', () => CanvasEditor.get())
class Tools extends React.Component {
  render() {
    const canvas = this.props.canvas

    const currentActiveTool = m.get(canvas, 'activeToolTarget')

    return (
      <div id='wf-tools' style={ styles }>
        { currentActiveTool ?
          <ActiveTool node={ currentActiveTool }
                      ctx={ m.get(canvas, 'activeToolContext') } /> : null
        }
        { !currentActiveTool && m.get(canvas, 'selectedTarget') ?
          <SelectedElement node={ m.get(canvas, 'selectedTarget') } /> : null
        }
        { !currentActiveTool && m.get(canvas, 'hoverTarget') ?
          <HoveredElement node={ m.get(canvas, 'hoverTarget') } /> : null
        }
      </div>
    )
  }
}

const styles = {
  pointerEvents: 'none',
  position: 'absolute',
  top: 0,
  left: 0,
  right: 0,
  bottom: 0
}

export default Tools

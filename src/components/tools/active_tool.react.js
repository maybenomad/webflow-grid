import GridResizer from 'components/tools/grid_resizer.react'

export default class ActiveTool extends React.Component {
  render() {
    return <GridResizer {...this.props} />
  }
}


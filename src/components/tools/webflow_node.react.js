import { getCanvasElement } from 'lib/dom'
import { ToolColor, border } from 'styles/colors'

const WF_PREFIX = 'w-tools-'

/*
 * A layout node for directly masking a node in the canvas layer.
 */
export default class WebflowNode extends React.Component {

  render() {
    let id, { canvasNode } = this.props

    // Avoid DOM traversals if we already have the information we need.
    if (canvasNode) {
      id = canvasNode.node.id
    }
    if (this.props.node) {
      id = m.get(this.props.node, '_id')
      canvasNode = getCanvasElement(id, this.props.absolute)
    }

    return (
      <div id={ WF_PREFIX + id } style={ _computeStyles(this.props, canvasNode) }>
        { this.props.children }
      </div>
    )
  }

}

function _computeStyles(props, canvasNode) {
  let propStyles = props.style

  // If a function is passed as the style prop, provide it all the data we've
  // collected from the DOM as well as its props and evaluate it.
  if (typeof propStyles === 'function') {
    propStyles = propStyles(props, canvasNode)
  }

  let borderStyle
  if (props.hovered) {
    borderStyle = border(1, 'solid', ToolColor.Hovered)
  }
  else if (props.selected) {
    borderStyle = border(1, 'solid', ToolColor.Selected)
  }

  return {
    // Position the node over its canvas counterpart.
    position: 'absolute',
    ...canvasNode.bounds,

    border: borderStyle,

    // Allow the owner to override any of these styles.
    ...propStyles
  }
}

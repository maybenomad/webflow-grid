import WebflowNode from 'components/tools/webflow_node.react'
import { ToolColor, border } from 'styles/colors'
import { attach } from 'lib/attach.react'
import * as DOM from 'lib/dom'
import * as _ from 'lib/collections'
import GridResizeMutator from 'mutators/grid_resize'
import { DOMNodeResizer } from 'processes/resizer'


@attach(DOMNodeResizer, {}, GridResizeMutator)
export default class GridResizer extends React.Component {
  render() {
    const { node } = this.props

    return (
      <WebflowNode absolute node={ node }>
        { _.dirtyMap(m.get(node, 'children'), childNode => {
          return <WebflowNode key={ m.get(childNode, '_id') }
                              node={ childNode }
                              style={ columnStyles } />
        })}
      </WebflowNode>
    )
  }
}

const columnStyles = (props, { node, parent, bounds }) => {
  const borderStyle = border(4, 'double', ToolColor.Selected)

  return {
    pointerEvents: 'auto',
    cursor: 'col-resize',
    height: parent.offsetHeight,
    borderLeft: borderStyle,
    borderRight: DOM.isLastChild(node, parent) ? borderStyle : null,
    left: bounds.left - 2
  }
}

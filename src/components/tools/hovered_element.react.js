import WebflowNode from 'components/tools/webflow_node.react'
import Column from 'components/tools/column.react'
import { getMainClass } from 'lib/dom_node'

export default class HoveredElement extends React.Component {
  render() {
    const { node } = this.props
    return  getMainClass(node) === 'w-col' ?
              <Column node={ node } /> :
              <WebflowNode absolute hovered node={ node } />
  }
}

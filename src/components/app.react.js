import Canvas from 'components/canvas/main.react'
import Tools from 'components/tools/main.react'

class App extends React.Component {
  render() {
    return (
      <div style={ styles }>
        <Canvas />
        <Tools />
      </div>
    )
  }
}

const styles = {
  position: 'absolute',
  width: '100%'
}

export default App

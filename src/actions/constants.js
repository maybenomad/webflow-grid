function defineActions(actions) {
  return actions.reduce((actionSet, action) => {
    return Object.assign(actionSet, { [ action ] : Symbol(action) })
  }, {})
}

const ActionTypes = defineActions([

  // Server events.
  'RECEIVED_DOM_NODES',
  'RECEIVED_DOM_STYLES',

  // Canvas DOM events.
  'RECEIVED_CANVAS_HOVER',
  'RECEIVED_CANVAS_CLICK',

  // Tool events.
  'CHANGED_ACTIVE_TOOL',
  'QUIT_ACTIVE_TOOL',
  'RESIZED_DOM_COLUMN'

])

export default ActionTypes

import Action from 'actions/constants'
import SiteData from 'json!../../webflow_data.json'
import dispatcher from 'lib/dispatcher'

const CanvasActions = {

  bootstrap() {
    // Lol I love destructuring
    let {
      pages: [,,,,, { nodes }],
      styles: { styles }
    } = SiteData

    dispatcher.dispatch(Action.RECEIVED_DOM_NODES, nodes)
    dispatcher.dispatch(Action.RECEIVED_DOM_STYLES, styles)
  },

  changedHoverTarget(id) {
    dispatcher.dispatch(Action.RECEIVED_CANVAS_HOVER, id)
  },

  clicked(id) {
    dispatcher.dispatch(Action.RECEIVED_CANVAS_CLICK, id)
  },

  changedActiveTool(targetId, ctx)  {
    dispatcher.dispatch(Action.CHANGED_ACTIVE_TOOL, { targetId, ctx })
  }

}

export default CanvasActions

var webpack = require('webpack')

var __DEV__ = !!process.env.BUILD_DEV

module.exports = {
  entry: [
    "webpack-dev-server/client?http://0.0.0.0:3000",
    "webpack/hot/only-dev-server",
    __dirname + "/src/entry.js"
  ],
  output: {
    path: __dirname + "/dist",
    publicPath: "/dist",
    filename: "bundle.js"
  },
  module: {
    loaders: [{
      test: /\.jsx?$/,
      exclude: /node_modules\/(?!js-csp)/,
      loaders: [ 'react-hot', 'babel-loader' ]
    }]
  },
  resolve: {
    root: __dirname + '/src'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),

    new webpack.ProvidePlugin({
      React: "react/addons",
      _: "mori",
      m: "mori"
    }),

    new webpack.DefinePlugin({
      __DEV__: __DEV__
    })
  ],
  devServer: {
    publicPath: '/dist',
    hot: true,
    historyApiFallback: true
  }
}
